// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("~/credentials/terraform.json")}"
 project     = "terraform-237713"
 region      = "us-west1"
}
// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
byte_length = 8
}
// A single Google Cloud Engine instance
resource "google_compute_instance" "first" {
 name         = "test1-vm"
 machine_type = "f1-micro"
 zone         = "us-west1-a"
 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }
 network_interface {
   network = "default"
   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}

resource "google_compute_instance" "second" {
 name         = "test2-vm"
 machine_type = "f1-micro"
 zone         = "us-west1-a"
 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }
 network_interface {
   network = "default"
   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}

resource "google_compute_instance" "three" {
 name         = "test3-vm"
 machine_type = "f1-micro"
 zone         = "us-west1-a"
 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }
 network_interface {
   network = "default"
   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}

